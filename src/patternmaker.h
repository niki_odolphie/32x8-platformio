uint8_t data[NUM_LEDS];

CRGBPalette16 gPalette(
    CRGB::Red, CRGB::Red,
    CRGB::Red, CRGB::Red, CRGB::Red, CRGB::White,
    CRGB::White, CRGB::White, CRGB::White, CRGB::White,
    CRGB::White, CRGB::Blue, CRGB::Blue, CRGB::Blue,
    CRGB::Blue, CRGB::Blue);

void render_data_with_palette()
{
    for (int i = 0; i < NUM_LEDS; i++)
    {
        leds[i] = ColorFromPalette(gPalette, data[i], 128, LINEARBLEND);
    }
}

void fill_data_array()
{
    static uint8_t startValue = 0;
    startValue = startValue + 2;

    uint8_t value = startValue;
    for (int i = 0; i < NUM_LEDS; i++)
    {
        data[i] = triwave8(value); // convert value to an up-and-down wave
        value += 7;
    }
}

void patternmaker()
{

    fill_data_array();
    render_data_with_palette();
}

void StripedPallet()
{
    currentPalette = RainbowStripeColors_p;
    static uint8_t startIndex = 0;
    startIndex = startIndex + 12; /* higher = faster motion */

    fill_palette(leds, NUM_LEDS,
                 startIndex, 8, /* higher = narrower stripes */
                 currentPalette, 255, LINEARBLEND);

    //FastLED.show();
    FastLED.delay(4000 / 30);
}

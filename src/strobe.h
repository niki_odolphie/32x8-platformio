void strobe()
{
    setAll(255, 255, 255);
    FastLED.show();
    delay(10);
    setAll(0, 0, 0);
    FastLED.show();
    delay(10);
}
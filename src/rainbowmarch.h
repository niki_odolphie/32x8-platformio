void rainbowmarch()
{
  uint8_t thishue = millis() * (255 - 150) / 255; // To change the rate, add a beat or something to the result. 'thisdelay' must be a fixed value.

  // thishue = beat8(50);                                       // This uses a FastLED sawtooth generator. Again, the '50' should not change on the fly.
  // thishue = beatsin8(50,0,255);                              // This can change speeds on the fly. You can also add these to each other.

  fill_rainbow(leds, NUM_LEDS, thishue, 10); // Use FastLED's fill_rainbow routine.
}
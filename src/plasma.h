
void Plasma()
{
    for (int i = 0; i < NUM_LEDS; i++)
    {
        byte c = sin8((long)i * 30 - millis() / 2);
        byte colorindex = scale8(c, 200);
        leds[i] = ColorFromPalette(currentPalette, colorindex);
    }
}
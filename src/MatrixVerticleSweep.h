void MatrixVerticleSweep()
{
    //setAll(0, 0, 0);
    fadeToBlackBy(leds, NUM_LEDS, 25);

    //leds[XY(31, 7)].setRGB(255, 20, 20);
    //HSV colours https://github.com/FastLED/FastLED/wiki/Pixel-reference
    //leds[XY(31, 7)].setHSV(224, 255, 255);

    int sweepColour;
    sweepColour = random8();

    for (int i = 0; i < kMatrixWidth; i++)
    {
        for (int j = 0; j < kMatrixHeight; j++)
        {
            //setAll(0, 0, 0);

            leds[XY(i, j)].setHSV(sweepColour, 255, 255);
            FastLED.show();
            //leds[XY(i, j)].setHSV(0, 0, 0);
            fadeToBlackBy(leds, NUM_LEDS, 2);

            leds[XY(kMatrixWidth - 1 - i, kMatrixHeight - 1 - j)].setHSV(sweepColour, 255, 255);
            FastLED.show();
            //leds[XY(i, j)].setHSV(0, 0, 0);
            fadeToBlackBy(leds, NUM_LEDS, 2);
        }
    }
}
void rainbow_centre()
{ // The fill_rainbow call doesn't support brightness levels.

    uint8_t thishue = millis() * (255 - 200) / 255; // To change the rate, add a beat or something to the result. 'thisdelay' must be a fixed value.

    uint8_t huemod = 175;

    fill_rainbow(leds, NUM_LEDS / 2, thishue, 10); // Use FastLED's fill_rainbow routine from the start to the center

    fill_rainbow(leds + NUM_LEDS / 2, NUM_LEDS / 2, thishue + huemod, 10 * -1); // And from the center to the far end

} // rainbow_center()
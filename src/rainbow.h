void rainbow()
{
  // FastLED's built-in rainbow generator
  //fill_rainbow( leds, NUM_LEDS, gHue, 7);

  uint8_t beatA = beatsin8(60, 0, 255); // Starting hue
  uint8_t beatB = beatsin8(60, 0, 255);
  fill_rainbow(leds, NUM_LEDS, (beatA + beatB) / 2, 8); // Use FastLED's fill_rainbow routine.
}
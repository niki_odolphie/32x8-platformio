void Sparkle()
{
    setAll(0, 0, 0);
    int Pixel = random(NUM_LEDS);
    setPixel(Pixel, 0xff, 0xff, 0xff);
    showStrip();
    delay(0);
    setPixel(Pixel, 0, 0, 0);
}
// PlatformIO version of the 32x8 matrix controller cycling between animations held in external files.

#include <Arduino.h>
#include <FastLED.h>

FASTLED_USING_NAMESPACE

#define kMatrixWidth 32
#define kMatrixHeight 8
#define LED_PIN 6
#define COLOR_ORDER GRB
#define CHIPSET WS2812B
#define BRIGHTNESS 255
#define NUM_LEDS (kMatrixWidth * kMatrixHeight)
#define kMatrixSerpentineLayout true
#define FRAMES_PER_SECOND 120
CRGB leds[NUM_LEDS];

uint8_t gCurrentTrackNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0;                // rotating "base color" used by many of the patterns
bool gRestartPlaylistFlag = false;
bool gLoopPlaylist = true;

CRGBPalette16 currentPalette = PartyColors_p;
CRGBPalette16 targetPalette = PartyColors_p;
TBlendType currentBlending = LINEARBLEND;

#include "utilityfunctions.h"

//Mostly patterns from https://www.tweaking4all.com/hardware/arduino/adruino-led-strip-effects/#LEDStripEffectSparkle
#include "dot_beat.h"
#include "juggle.h"
#include "matrix_ray.h"
#include "noisePattern01.h"
#include "pride.h"
#include "rainbow.h"
#include "rainbowmarch.h"
#include "sinelon.h"
#include "cylon.h"
#include "heartbeat.h"
#include "sparkle.h"
#include "RGBloop.h"       //Why does this run for so long?
#include "RunningLights.h" //Why does this run for so long?  Taken out of the list for nows
#include "plasma.h"
#include "StripedPallet.h"
#include "patternmaker.h"
#include "strobe.h"
#include "MatrixPulse.h"
#include "MatrixVerticleSweep.h"
#include "basicfadein.h"
#include "RainbowCentre.h"
#include "Flipbook.h"
#include "ScrollingPatterns.h"

// From https://gist.github.com/kriegsman/841c8cd66ed40c6ecaae
typedef void (*SimplePattern)();
typedef SimplePattern SimplePatternList[];
typedef struct
{
  SimplePattern mPattern;
  uint16_t mTime;
} PatternAndTime;
typedef PatternAndTime PatternAndTimeList[];
const PatternAndTimeList gPlaylist = {
    //{RunningLights, 1},
        {Scroll_Pattern_Slider, 10},

    {Scroll_Pattern_Fuck, 10},
    {Scroll_Pattern_Zigzag, 10},
    {Flipbook, 5},
    {rainbow_centre, 10},
    {BasicFadeIn, 10},
    //{cylon, 1},
    {MatrixVerticleSweep, 15},
    {RGBLoop, 7},
    {MatrixPulse, 10},
    {pride, 15},
    {juggle, 20},
    {Plasma, 10},
    {strobe, 3},
    {patternmaker, 15},
    {StripedPallet, 15},
    {Plasma, 15},
    {Sparkle, 10},
    {heartBeat, 15},
    {dot_beat, 15},
    {matrix_ray, 15},
    {pride, 15},
    {rainbow, 5},
    {rainbowmarch, 15},
    {sinelon, 20},

};

void RestartPlaylist()
{
  gRestartPlaylistFlag = true;
}

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))
void nextPattern()
{
  // add one to the current pattern number
  gCurrentTrackNumber = gCurrentTrackNumber + 1;
  // If we've come to the end of the playlist, we can either
  // automatically restart it at the beginning, or just stay at the end.
  if (gCurrentTrackNumber == ARRAY_SIZE(gPlaylist))
  {
    if (gLoopPlaylist == true)
    {
      // restart at beginning
      gCurrentTrackNumber = 0;
    }
    else
    {
      // stay on the last track
      gCurrentTrackNumber--;
    }
  }
}

void setup()
{
  delay(1000); //Time for the board to setup
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 250);
  LEDS.addLeds<CHIPSET, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  LEDS.setBrightness(BRIGHTNESS);

  InitialiseNoise(); //in noisepattern01.h

  RestartPlaylist();
}

void loop()
{

  // Call the current pattern function once, updating the 'leds' array
  gPlaylist[gCurrentTrackNumber].mPattern();
  // send the 'leds' array out to the actual LED strip
  FastLED.show();
  // insert a delay to keep the framerate modest
  FastLED.delay(250 / FRAMES_PER_SECOND);

  //if (Serial.read() == 'r')
  //  RestartPlaylist();

  // do some periodic updates
  EVERY_N_MILLISECONDS(20) { gHue++; } // was gHue++ slowly cycle the "base color" through the rainbow

  {
    EVERY_N_SECONDS_I(patternTimer, gPlaylist[gCurrentTrackNumber].mTime)
    {
      nextPattern();
      patternTimer.setPeriod(gPlaylist[gCurrentTrackNumber].mTime);
    }
    // Here's where we handle restarting the playlist if the 'reset' flag
    // has been set. There are a few steps:
    if (gRestartPlaylistFlag)
    {
      gCurrentTrackNumber = 0;
      // Set the playback duration for this pattern to it's correct time
      patternTimer.setPeriod(gPlaylist[gCurrentTrackNumber].mTime);
      // Reset the pattern timer so that we start marking time from right now
      patternTimer.reset();
      // Finally, clear the gRestartPlaylistFlag flag
      gRestartPlaylistFlag = false;
    }
  }
}

void RGBLoop()
{

    setAll(255, 0, 0);
    FastLED.show();
    delay(200);
    setAll(0, 255, 0);
    delay(200);
    setAll(0, 255, 255);
    FastLED.show();
    delay(200);
}
// Define variables used by the sequences.
int thisdelay = 10;    // A delay value for the sequence(s)
uint8_t count = 0;     // Count up to 255 and then reverts to 0
uint8_t fadeval = 150; // Trail behind the LED's. Lower => faster fade.

uint8_t bpm = 60;

void dot_beat()
{

  uint8_t inner = beatsin8(bpm, NUM_LEDS / 4, NUM_LEDS / 4 * 3);  // Move 1/4 to 3/4
  uint16_t outer = beatsin16(bpm, 0, NUM_LEDS - 1);               // Move entire length
  uint8_t middle = beatsin8(bpm, NUM_LEDS / 3, NUM_LEDS / 3 * 2); // Move 1/3 to 2/3

  leds[middle] = CRGB::Purple;
  leds[inner] = CRGB::Blue;
  leds[outer] = CRGB::Aqua;

  nscale8(leds, NUM_LEDS, fadeval); // Fade the entire array. Or for just a few LED's, use  nscale8(&leds[2], 5, fadeval);

} // dot_beat()